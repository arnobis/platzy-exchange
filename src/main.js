import { createApp } from "vue";
import App from "./App.vue";
import "@/assets/css/tailwind.css";

import router from "@/router";
import { dollarFilter, percentFilter } from "@/filters";

createApp(App).use(router).use(dollarFilter).use(percentFilter).mount("#app");
