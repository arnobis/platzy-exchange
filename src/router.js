import { createWebHistory, createRouter } from "vue-router";
import HomeComponent from "@/views/Home.vue";
import AboutComponent from "@/views/About.vue";
import ErrorComponent from "@/views/Error.vue";
import CoinDetailComponent from "@/views/CoinDetail.vue";

const history = createWebHistory();

export default createRouter({
  history,

  routes: [
    {
      path: "/",
      name: "home",
      component: HomeComponent,
    },
    {
      path: "/about",
      name: "aboutComponent",
      component: AboutComponent,
    },
    {
      path: "/coin/:id(.*)",
      name: "coin-Detail",
      component: CoinDetailComponent,
    },
    {
      path: "/:catchAll(.*)",
      name: "error",
      component: ErrorComponent,
    },
  ],
});
